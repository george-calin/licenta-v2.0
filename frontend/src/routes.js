import React from 'react';
import { Route } from 'react-router-dom';
import Login from './containers/LoginPage';
import Signup from './containers/Signup';
import History from './containers/History';
import HomePage from './containers/HomePage';

const BaseRouter = () => (
    <div>
        <Route exact path='/' component={HomePage} />
        <Route exact path='/login/' component={Login} />
        <Route exact path='/signup/' component={Signup} />
		<Route exact path='/history/' component={History} />
    </div>
);

export default BaseRouter;