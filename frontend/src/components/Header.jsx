import React from 'react';
import { Layout, Modal, message, Button, Icon  } from 'antd';
import { Row, Col } from 'react-flexbox-grid'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Logo from './Logo';
import DropdownLogin from './DropdownLogin';

const { Header } = Layout;

class CustomHeader extends React.Component {

	state = {
		ModalText: 'Are you sure you want to logout?',
		modalVisibility: false,
		confirmLoading: false,
		isAuthenticated: null,
	}


	componentWillReceiveProps(newProps) {
		if (newProps.isAuthenticated)
		{
			this.setState({
				isAuthenticated: newProps.isAuthenticated
			});
		}
	};

	showModal = () => {
		this.setState({
		  visible: true,
		});
	};

	handleOk = () => {
		this.setState({
			confirmLoading: true,
		});
		message.success('Logout succesfuly');
		setTimeout(() => {
			this.setState({
				visible: false,
				confirmLoading: false,
			});
			this.props.logout();
			window.location.reload();
		}, 300);
	};

	handleCancel = () => {
		console.log('Clicked cancel button');
		this.setState({
			visible: false,
		});
	};

	render() {

	const { visible, confirmLoading, ModalText } = this.state;
		return (
		<div>
			<Header className="header"
					style={{
						backgroundColor: "#7e3ff2",
						borderColor: "#5300e8",
						color: "#fff",
						minHeight: 50}}
			>

				<Row middle="xs">
					<Col xs={2}  style={{float:"left"}}>
						<Logo/>
					</Col>
					<Col xs={10}>
						<Row style={{float:"right"}}>
							{
								this.state.isAuthenticated ?
								(
									<DropdownLogin onClick={this.showModal}/>
								)
								:
								<Button type="danger">
									<Link to="/login/" >
										<Icon type="user" style={{marginRight: "5px" }}/>
										 Log-in
									</Link>
								</Button>
							}
						</Row>
					</Col>
				</Row>
			</Header>
			<Modal
				title="Confirm Logout"
				visible={visible}
				onOk={this.handleOk}
				confirmLoading={confirmLoading}
				onCancel={this.handleCancel}
			>
				<p>{ModalText}</p>
			</Modal>
		</div>
		);
	}
}

const mapStateToProps = (state) => {
    return {
		isAuthenticated: state.token !== null
    }
}

// export default CustomHeader;

export default connect(mapStateToProps, null)(CustomHeader);