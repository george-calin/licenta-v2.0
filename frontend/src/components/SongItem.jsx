import React from 'react';
import { Tag } from 'antd';

class SongItem extends React.Component {
    render() {
        return (
            <Tag style={{width: "100%", overflow: "hidden"}} color={this.props.color}><h1>{this.props.text}</h1></Tag>
        )
    }
}

export default SongItem;
