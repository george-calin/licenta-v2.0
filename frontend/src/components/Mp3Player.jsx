import React from 'react';
import ReactPlayer from 'react-player';
import { Icon, Row, Col } from 'antd';
import {connect} from 'react-redux';
import ImageLoader from './Images';
import '../styles/Mp3Player.css';



class Mp3Player extends React.Component {

    constructor(props) {
        super(props);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.onPlay = this.onPlay.bind(this);
        this.onPause = this.onPause.bind(this);
    }

    state = {
        songIndex: 0,
        images: [],
        songs: this.props.songs,
        playing: false
    }

    next() {
        if(this.state.songs.length > 0)
        {
            const nextSongIndex = (this.state.songs.length - 1 !== this.state.songIndex) ? this.state.songIndex + 1 : 0;
            this.setState({
                songIndex: nextSongIndex,
            });
        }
    }

    previous() {
        if(this.state.songs.length > 0)
        {
            const prevSongIndex = (this.state.songIndex !== 0) ?  this.state.songIndex -1 : this.state.songs.length - 1;
            this.setState({
                songIndex: prevSongIndex,
            });
        }
    }

    onPlay() {
        this.setState({
            playing: true
        })
    }

    onPause() {
        this.setState({
            playing: false
        })
    }

    componentDidMount() {
        const images = ImageLoader()
        // this.setState({
        //     images: images
        // })
        // if(this.props.selectedSong != null)
        // {
        //     let tempSongs = this.props.songs.filter(song => song.id === this.props.selectedSong);
        //     this.setState({
        //         images: images,
        //         songs: tempSongs,
        //         songIndex: 0
        //     })
        // }
        // else if(this.props.selectedGenre != null)
        // {
        //     let tempSongs = this.props.songs.filter(song => song.genre.genre_id === this.props.selectedGenre);
        //     this.setState({
        //         images: images,
        //         songs: tempSongs,
        //         songIndex: 0
        //     })
        // }
        this.setState({
            images: images,
            songs: this.props.songs,
            songIndex: 0
        })
    }

    componentWillReceiveProps(newProps) {
        // if(newProps.selectedSong != null)
        // {
        //     let tempSongs = newProps.songs.filter(song => song.id === newProps.selectedSong);
        //     this.setState({
        //         songs: tempSongs,
        //         songIndex: 0
        //     })
        // }   
        // else if(newProps.selectedGenre != null)
        // {   
        //     let tempSongs = newProps.songs.filter(song => song.genre.genre_id === newProps.selectedGenre);
        //     this.setState({
        //         songs: tempSongs,
        //         songIndex: 0
        //     })
        // }
        // else 
        // {
        //     this.setState({
        //         songs: newProps.songs
        //     });
        // }
        this.setState({
            songs: newProps.songs,
            songIndex: newProps.selectedSong
        })
    }

    render() {
        let songURL, songName, genre;
        // something aint right
        if( this.state.songs.length === 0)
        {
            songURL = null;
            songName = "No song found" 
            genre = {
                        "genre_id": null,
                        "genre_name": null,
                    };
        }
        // something is right
        else 
        {
            songURL   = this.state.songs[this.state.songIndex].file
            songName  = this.state.songs[this.state.songIndex].name
            genre     = this.state.songs[this.state.songIndex].genre
        }

        return (
            <Row type="flex" justify="space-between" align="middle" className='player-wrapper'>
                <Col xs={{span: 1}} sm={{span: 2}} md={{span: 2}} lg={{ span: 2}} xl={{span:2}} >
                    <Icon type="step-backward"  onClick={this.previous}  className="left-button" style={{fontSize: "3vw" }} />
                </Col>
                <Col xs={{span: 22}} sm={{span: 20}} md={{span: 20}} lg={20} xl={{span:20}}>
                    <div style={{ position:"absolute", color: "white", textAlign: "center", width:"100%", top:"5%", fontSize: "1.5vw", lineHeight: "2vw" }}>
                        <Row><Col lg={24}>{songName}</Col></Row>
                        {/* <Row><Col lg={24}>{genre.name}</Col></Row> */}
                    </div>

                    <ReactPlayer
                        className={this.props.className}        
                        url={songURL}
                        width={this.props.width}
                        height={this.props.height}
                        controls={true}
                        playing={this.state.playing}
                        playsinline={true}
                        pip={true}
                        onPlay={this.onPlay}
                        onPause={this.onPause}
                        config={{
                            file: {
                                attributes: {
                                    poster: this.state.images[genre.genre_id],
                                }
                            }
                        }}
                    />
                </Col>
                <Col xs={{span: 1}} sm={{span: 2}} md={{span: 2}} lg={{ span: 2}} xl={{span:2}}>
                    <Icon type="step-forward"  onClick={this.next}  className="right-button" style={{fontSize: "3vw" }}/>
                </Col>
            </Row>
        );
    }
}


// export default Mp3Player;

const mapStateToProps = state => {
	return {
        genres: state.genres,
        songs: state.songs,
        selectedGenre: state.selectedGenre,
        selectedSong: state.selectedSong
	}
  }

export default connect(mapStateToProps, null)(Mp3Player);
