import React from 'react';
import { Row} from 'antd';
import Mp3Player from './Mp3Player';


class MyCarousel extends React.Component {


    componentDidUpdate() {
    }

    render() {
        const playerProps = {
            className:'react-player',
            width:'100%',
            height: '20vw',
        }

        return (
            <Row  style={{ background: 'black'}}>
                <Mp3Player {...playerProps}/>
            </Row>
        );
    }
}

export default MyCarousel;