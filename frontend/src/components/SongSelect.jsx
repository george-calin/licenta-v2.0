import React from 'react';
import {connect} from 'react-redux';
import { Select } from 'antd';
import * as actions from '../store/actions/filters';

const { Option } = Select;

class SongSelect extends React.Component {
    constructor(props) {
        super(props);
        this.onChange   = this.onChange.bind(this);
        this.onBlur     = this.onBlur.bind(this);
        this.onFocus    = this.onFocus.bind(this);
        this.onSearch   = this.onSearch.bind(this);
    }
    state = {
        children: []
    }

    onChange(value) {
        value = value !== undefined ? Number(value) : 0; 
        this.props.changeSelectedSong(value);
    }
    
    onBlur() {
        // console.log('blur'); 
    }
    
    onFocus() {
        // console.log('focus');
    }
    
    onSearch(val) {
        // console.log('search:', val);
    }
    componentDidMount() {
        var tempChildren = []
        this.props.songs.forEach(function(item, index, array) {
            // const child = <Option key={item.id}>{item.name}</Option>;
            const child = <Option key={index}>{item.name}</Option>;
            tempChildren.push(child)
        });
        this.setState({
            children: tempChildren
        });
    }

    componentWillReceiveProps(newProps) {
        var tempChildren = []
        newProps.songs.forEach(function(item, index, array) {
            // const child = <Option key={item.id}>{item.name}</Option>;
            const child = <Option key={index}>{item.name}</Option>;
            tempChildren.push(child)
        });
        this.setState({
            children: tempChildren
        });
    }
    render() {
        const selectedValue = this.props.selectedSong ? this.props.selectedSong : null;   
        return (
            <Select
                showSearch
                style={{ width: "100%" }}
                placeholder="Search a song"
                optionFilterProp="children"
                onChange={this.onChange}
                onFocus={this.onFocus}
                onBlur={this.onBlur}
                onSearch={this.onSearch}
                size="large"
                // value={selectedValue}
                // defaultValue={ this.props.selectedSong!= null ? String(this.props.selectedSong) : null}
                filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                allowClear={true}
                >
                {this.state.children}
            </Select>
        )
    }
}

const mapStateToProps = state => {
	return {
        selectedSong: state.selectedSong,
        selectedGenre: state.selectedGenre,
        songs: state.songs,
	}
}

const mapDispatchToProps = dispatch => {
    return {
        changeSelectedSong: (val) => dispatch(actions.changeSelectedSong(val))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SongSelect);


