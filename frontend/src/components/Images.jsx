import RapImg from '../images/genres/Rap.jpg';
import RockImg from '../images/genres/Rock.jpg';
import PopImg from '../images/genres/Pop.jpg';
import ClassicalImg from '../images/genres/Classical.jpg';
import ElectronicImg from '../images/genres/Electronic.jpg';
import ReggaeImg from '../images/genres/Reggae.jpg';

const images = [
    RapImg, ElectronicImg, PopImg, RockImg, ClassicalImg, ReggaeImg
];

const ImageLoader = () => {
    return images;
}

export default ImageLoader;
