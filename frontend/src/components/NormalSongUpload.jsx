import React from 'react';
import { Upload, Button, Icon, Alert, Row } from 'antd';
import * as actions from '../store/actions/uploadFile';
import { connect } from 'react-redux';

class NormalSongUpload extends React.Component {

	constructor(props) {
		super(props)
		this.state = {
			errorMessage: (<Alert
				type="error"
				message="Server error"
				style={{marginTop: "15px"}}
			/>)
		}
	}
	handleFileUpload = () => {
		const file = this.props.fileList[0];
		this.props.uploadRequest({
			file: file,
			token: this.props.token
		});

	}

	render() {
		const { uploading, fileList, error } = this.props;
		const props = {
			onRemove: file => {
				this.props.tempRemoveFile();
			},
			beforeUpload: file => {
				this.props.tempUploadFile(file);
				return false;
			},
			listType: 'picture',
			previewFile(file) {
				console.log('Your upload file:', file);
				// Your process logic. Here we just mock to the same file
				return fetch('https://next.json-generator.com/api/json/get/4ytyBoLK8', {
						method: 'POST',
						body: file,
					})
					.then(res => res.json())
					.then(({ thumbnail }) => thumbnail);
			},
			multiple: false,
			fileList: fileList
		};

		return (
            <div>
            {
                uploading ?
                (
                <Row>
                    {/* <Spin style={{textAlign:"center"}} indicator={loadingIcon} /> */}
                </Row>
                )
                :
                (
                <div>
                    <Upload {...props}>
                    <Button>
                        <Icon type="upload" /> Select File
                    </Button>
                    </Upload>
                </div>
                )
            }

            <Button
				type="primary"
				onClick={this.handleFileUpload}
				disabled={fileList.length === 0}
				loading={uploading}
				style={{ marginTop: 16 }}
			>
            {uploading ? 'Uploading' : 'Start Upload'}
            </Button>
			
            {error ? this.state.errorMessage : null}


            </div>
		);
	}
}

const mapStateToProps = (state) => {
    return {
        uploading: state.uploadingFile,
		error: state.errorFile,
		token: 	state.token,
		fileList: (state.tempFile) ? [state.tempFile] : []
	}
}

const mapDispatchToProps = dispatch => {
    return {
		tempRemoveFile: () 				=> dispatch(actions.tempUploadRemove()),
		tempUploadFile: (file) 			=> dispatch(actions.tempUploadFile(file)),
		uploadRequest: (file, token) 	=> dispatch(actions.uploadDocumentRequest(file, token))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NormalSongUpload);
