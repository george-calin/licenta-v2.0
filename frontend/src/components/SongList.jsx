import React from 'react';
import {Row, Col} from 'antd';
import { connect } from 'react-redux';
import SongItem from '../components/SongItem';

const colors = [
    "magenta",
    "red",
    "volcano",
    "orange",
    "gold",
    "lime",
    "green",
    "cyan",
    "blue",
    "geekblue",
    "purple"
]

class SongList extends React.Component {
    getRandomColor() {
        return colors[Math.floor(Math.random() * Math.floor(colors.length))]
    }
    
    render() {
        const { songs } = this.props;
        
        let body    = [],
            length  = songs.length;

        for ( let index=0; index<length ; index+=3 )
        {
            body.push(
                <Row 
                    type="flex" 
                    justify="space-around" 
                    gutter={16} 
                    key={songs[index].id} 
                >
                    <Col lg={{span: 8}} md={24} style={{marginBottom: '15px'}}>
                        <SongItem text={songs[index].name} color={this.getRandomColor()} />
                    </Col>
            {   index + 1 < length ? 
                (
                    <Col lg={{span: 8}} md={24} style={{marginBottom: '15px'}}>
                        <SongItem text={songs[index+1].name} color={this.getRandomColor()} />
                    </Col>      
                ) : null
            }        
            {   index + 2 < length ?
                (
                    <Col lg={{span: 8}} md={24} style={{marginBottom: '15px'}}>
                        <SongItem text={songs[index + 2].name} color={this.getRandomColor()} />
                    </Col> 
                ) : null
            }     
                </Row>
            )
        }

        return (
            <div >{body}</div>
        )
    }
}

const mapStateToProps = state => {
    return {
        songs: state.recomendedSongs
    }
}

export default connect(mapStateToProps, null)(SongList);


    // import { Row, Col } from 'antd';

    // ReactDOM.render(
    //     <div>
    //         <p>sub-element align left</p>
    //         <Row type="flex" justify="start">
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //         </Row>

    //         <p>sub-element align center</p>
    //         <Row type="flex" justify="center">
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //         </Row>

    //         <p>sub-element align right</p>
    //         <Row type="flex" justify="end">
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //         </Row>

    //         <p>sub-element monospaced arrangement</p>
    //         <Row type="flex" justify="space-between">
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //         </Row>

    //         <p>sub-element align full</p>
    //         <Row type="flex" justify="space-around">
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //             <Col span={4}>col-4</Col>
    //         </Row>
    //     </div>,
    //     mountNode,
    // );