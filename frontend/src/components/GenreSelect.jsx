import React from 'react';
import {connect} from 'react-redux';
import { Select } from 'antd';
import * as actions from '../store/actions/filters';

const { Option } = Select;

class GenreSelect extends React.Component {
    constructor(props) {
        super(props);
        this.onChange   = this.onChange.bind(this);
        this.onBlur     = this.onBlur.bind(this);
        this.onFocus    = this.onFocus.bind(this);
        this.onSearch   = this.onSearch.bind(this);
    }
    state = {
        children: []
    }

    onChange(value) {
        value = value !== undefined ? Number(value) : null; 
        this.props.changeSelectedSong(null);
        this.props.changeSelectedGenre(value, this.props.songsBackup);
    }
    
    onBlur() {
    }
    
    onFocus() {
    }
    
    onSearch(val) {
    }

    componentDidMount() {
        var tempChildren = []
        this.props.genres.forEach(function(item, index, array) {
            const child = <Option key={index}>{item.name}</Option>;
            tempChildren.push(child)
        });
        this.setState({
            children: tempChildren
        }); 
        this.onChange(undefined);
    }

    // componentWillReceiveProps(newProps) {
    //     // populate simport * as actions from '../store/actions/auth';elect with children
    //     var tempChildren = []
    //     newProps.genres.forEach(function(item, index, array) {
    //         const child = <Option key={index}>{item.name}</Option>;
    //         tempChildren.push(child)
    //     });
    //     this.setState({
    //         children: tempChildren
    //     });
	// }
    
    render() { 
        return (
            <Select
                showSearch
                style={{ width: "100%" }}
                placeholder="Select a genre"
                optionFilterProp="children"
                onChange={this.onChange}
                onFocus={this.onFocus}
                onBlur={this.onBlur}
                onSearch={this.onSearch}
                size="large"
                // defaultValue={ this.props.selectedGenre!= null ? String(this.props.selectedGenre) : null}
                filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                allowClear={true}
                >
                {this.state.children}
            </Select>
        )
    }
}

const mapStateToProps = state => {
    return {
        selectedGenre: state.selectedGenre,
        genres: state.genres,
        songsBackup: state.songsBackup
	}
}

const mapDispatchToProps = dispatch => {
    return {
        changeSelectedSong: (val) => dispatch(actions.changeSelectedSong(val)),
        changeSelectedGenre: (genre, songsBackup) => dispatch(actions.changeSelectedGenre(genre, songsBackup))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GenreSelect);


