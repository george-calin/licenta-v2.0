// import React from 'react';
// import { Carousel, Row, Col, Icon } from 'antd';
// import {connect} from 'react-redux';
// import Mp3Player from './Mp3Player';
// import '../styles/Carousel.css'


// class MyCarousel extends React.Component {

//     constructor(props) {
//         super(props);
//         this.next = this.next.bind(this);
//         this.previous = this.previous.bind(this);
//         this.carousel = React.createRef();
//     }

//     next() {
//         this.carousel.next();
//     }

//     previous() {
//         this.carousel.prev();
//     }

//     componentDidUpdate() {
//     }

//     render() {
//         const props = {
//             dots: true,
//             infinite: true,
//             speed: 500,
//             slidesToShow: 1,
//             slidesToScroll: 1
//         };

//         const playerProps = {
//             className:'react-player',
//             url:'http://www.youtube.com/watch?v=hHW1oY26kxQ',
//             width:'800px',
//             height:'440px',
//             dots: false,
//             light: true,
//             playsinline: true
//         }

//         return (
//             <Row gutter={16} style={{ background: 'black'}}>
//                 <Col md={{ span: 24 }}  lg={{ span: 16 }} offset={4} style={{backgroundColor: "black" }}>
//                     <Row>
//                         <Col span={22} offset={1}>
//                             <Icon type="step-backward"  onClick={this.previous}  className="left-button" />
//                             <Carousel ref={node => (this.carousel = node)} {...props} dots={false}>
//                                 <div>
//                                     <Mp3Player {...playerProps}/>
//                                 </div>
//                                 <div>
//                                     <Mp3Player {...playerProps}/>
//                                 </div>
//                             </Carousel>
//                             <Icon type="step-forward"  onClick={this.next}  className="right-button" />
//                         </Col>
//                     </Row>
//                 </Col>
//                 <Col md={{ span: 24 }} lg={{ span: 12 }} style={{backgroundColor: "black" }}>
//                 </Col>
//             </Row>
//         );
//     }
// }


// const mapStateToProps = state => {
// 	return {
// 	  songs: state.songs
// 	}
//   }

// // before
// export default connect(mapStateToProps, null)(MyCarousel);
