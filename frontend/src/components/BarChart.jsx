import React, { Component } from 'react';
import Chart from 'react-apexcharts';

class BarChart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            options: {
                chart: {
                    type: 'bar',
                },
                dataLables: {
                    enabled: false,
                },
                xaxis: {
                    categories: []
                },
                noData: {
                    text: 'No Data Found',
                    align: 'center',
                    verticalAlign: 'middle',
                    offsetX: 0,
                    offsetY: 0,
                    style: {
                        color: 'gray',
                        fontSize: '4em',
                        fontFamily: undefined
                    }
                },
                title: {
                    text: '',
                    align: 'left',
                    margin: 10,
                    offsetX: 10,
                    offsetY: 0,
                    floating: false,
                    style: {
                        fontSize: '30px',
                        fontFamily: undefined,
                        color: undefined,
                    },
                },
            },
            series: [{
                name: "Count",
                data: []
            }]
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            options: {
                ...this.state.options,
                xaxis: {
                    ...this.state.options.xaxis,
                    categories: newProps.categories
                }
            },
            series: [{
                ...this.state.series,
                data: newProps.data
            }],
        })
    }

    render() {
        return (
            <Chart options={this.state.options} series={this.state.series} type="bar" width={this.props.width} height={this.props.height} />
        )
    }
}

export default BarChart;