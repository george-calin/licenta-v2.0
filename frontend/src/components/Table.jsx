import React from 'react';
import { Table, Icon } from 'antd';
import { connect } from 'react-redux';
import * as actions from '../store/actions/filters';



class MyTable extends React.Component {

    onClickPlay = value => {
        value = value !== undefined ? Number(value) : null;
        this.props.changeSelectedSong(value);
    }
    

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            categories: [],
            columns: [
                {
                    title: '',
                    dataIndex: 'index',
                    key: 'index',
                    render: index => (
                        <span>
                            <a href="javascript:;" onClick={() => this.onClickPlay(index)}>
                                <Icon
                                    type="play-circle"
                                    theme="outlined"
                                    twoToneColor="#7E3FF2"
                                    style={{ fontSize: "1.5vw", color: "#7E3FF2" }}
                                />
                            </a>
                        </span>
                    ),
                },
                {
                    title: 'TITLE',
                    dataIndex: 'title',
                    key: 'title',
                },
                {
                    title: 'GENRE',
                    dataIndex: 'genre',
                    key: 'genre',
                },
            ],
        }
    }

    render() {
        return (
            < Table columns={this.state.columns} dataSource={this.props.dataSource} pagination={{ pageSize: 6 }} size="middle" scroll={{y: "17.6vw"}}/>
        )
    }
}


const mapDispatchToProps = dispatch => {
    return {
        changeSelectedSong: (val) => dispatch(actions.changeSelectedSong(val))
    }
}

export default connect(null, mapDispatchToProps)(MyTable);

