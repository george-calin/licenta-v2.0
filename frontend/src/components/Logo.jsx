import React from 'react';
import logo from '../images/googlemusic.png'
import { NavLink } from 'react-router-dom';

const Logo = props => (
	<NavLink style={{marginRight: '10px'}} to='/'>
		<img style={{height:"55px"}} alt="Logo" src={logo}/>
	</NavLink>
)

export default Logo;
