import React from 'react';
import { Form, Input, Icon, Button, Alert } from 'antd';
import { connect } from 'react-redux';
import * as actions from '../store/actions/auth';
import axios from 'axios';

const FormItem = Form.Item;

class RegistrationForm extends React.Component {
  state = {
    confirmDirty: false,
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        // this.props.onAuth(
        //     values.userName,
        //     values.email,
        //     values.password,
        //     values.confirm
        // );
        // this.props.history.push('/');
        this.props.authStart();
        setTimeout(() => {
          axios.post('http://127.0.0.1:8000/rest-auth/registration/', {
            username: values.userName,
            email: values.email,
            password1: values.password,
            password2: values.confirm
          })
            .then(res => {
              const token = res.data.key;
              const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
              const data = {
                token: token,
                username: values.userName,
                password: values.password,
                expirationDate: expirationDate
              }
              this.props.authLogin(data);
              this.props.history.push('/');
            })
            .catch(err => {
              this.props.signinFail(err['response'].data);
            })
        }, 200);
      }
    });
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }


  render() {
    const { getFieldDecorator } = this.props.form;
    const { error } = this.props;
    let errorMessage = null;
    if (error) {
      let errorMessages = []
      for(let property in error){
        errorMessages.push(
          <Alert
            type="error"
            message={error[property]}
            style={{ marginBottom: "10px" }}
          />
        )
      }
      errorMessage = (
        <div>
          {errorMessages}
        </div>
      );
    }
    return (
      <div style={{width:"80%"}}>
          <Form onSubmit={this.handleSubmit}>

            <FormItem>
                {getFieldDecorator('userName', {
                    rules: [{ required: true, message: 'Please input your username!' }],
                })(
                    <Input size="large" prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                )}
            </FormItem>

            <FormItem>
              {getFieldDecorator('email', {
                rules: [{
                  type: 'email', message: 'The input is not valid E-mail!',
                }, {
                  required: true, message: 'Please input your E-mail!',
                }],
              })(
                <Input size="large" prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
              )}
            </FormItem>

            <FormItem>
              {getFieldDecorator('password', {
                rules: [{
                  required: true, message: 'Please input your password!',
                }, {
                  validator: this.validateToNextPassword,
                }],
              })(
                <Input size="large" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
              )}
            </FormItem>

            <FormItem>
              {getFieldDecorator('confirm', {
                rules: [{
                  required: true, message: 'Please confirm your password!',
                }, {
                  validator: this.compareToFirstPassword,
                }],
              })(
                <Input size="large" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" onBlur={this.handleConfirmBlur} />
              )}
            </FormItem>

            <FormItem>
            <Button type="primary" htmlType="submit" style={{marginRight: '10px'}}>
                Signup
            </Button>
            </FormItem>
            {errorMessage}
          </Form>
        </div>
    );
  }
}

const WrappedRegistrationForm = Form.create()(RegistrationForm);

const mapStateToProps = (state) => {
    return {
        loading: state.loading,
        error: state.errorSignup
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, email, password1, password2) => dispatch(actions.authSignup(username, email, password1, password2)),
        authLogin: (username, password) => dispatch(actions.authLogin(username, password)),
        authStart: () => dispatch(actions.authStart()),
        signinFail: (error) => dispatch(actions.signinFail(error)),
      }
}

export default connect(mapStateToProps, mapDispatchToProps)(WrappedRegistrationForm);
