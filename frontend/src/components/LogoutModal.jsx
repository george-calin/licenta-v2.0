import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Modal } from 'antd';
import * as actions from '../store/actions/auth';

class LogoutModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			ModalTitle: 'Confirm logout',
			ModalText: 'Are you sure you want to logout?',
			modalVisibility: false,
			confirmLoading: false,
		}
	}

	showModal = () => {
		this.setState({
		  visible: true,
		});
	};

	handleOk = () => {
		this.setState({
			confirmLoading: true,
		});

		setTimeout(() => {
			this.setState({
				visible: false,
				confirmLoading: false,
			});
			this.props.logout();
			window.location.reload();
		}, 100);
	};

	handleCancel = () => {
		console.log('Clicked cancel button');
		this.setState({
			visible: false,
		});
	};

	render () {
		return (
			<Modal
				title={this.state.ModalTitle}
				visible={this.state.visible}
				onOk={this.handleOk}
				confirmLoading={this.state.confirmLoading}
				onCancel={this.handleCancel}
			>
			<p>{this.state.ModalText}</p>
			</Modal>
		);
	}
}

const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(actions.logout()) 
    }
}

export default withRouter(connect(null, mapDispatchToProps)(LogoutModal));