import React from 'react';
import { Menu, Dropdown, Button, Icon } from 'antd';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';


class DropdownLogin extends React.Component {
	render () {
		return (
			<Dropdown
				overlay={
						<Menu>
						<Menu.Item key="1">
							<Link to="/login/" >
								<Icon type="login"/> Switch account
							</Link>
						</Menu.Item>
						<Menu.Item key="2" onClick={this.props.onClick}>
							<Icon type="logout"/>Log-out
						</Menu.Item>
						</Menu>
				}
			>
				<Button type="primary"
						style={{backgroundColor: "#7e3ff2",
								borderColor: "#7e3ff2"}}>
					<Icon type="user" />
					Hello, { this.props.username }
					<Icon type="caret-down" />
				</Button>
			</Dropdown>
		);
	}
}

const mapStateToProps = (state) => {
    return {
        username: state.username,
    }
}

export default connect(mapStateToProps, null)(DropdownLogin);
