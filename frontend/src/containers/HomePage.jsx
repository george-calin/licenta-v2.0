import React from 'react';
import {connect} from 'react-redux';
import {Row, Col, Card, Icon} from 'antd';
import NormalSongUpload from '../components/NormalSongUpload';
import SongList from '../components/SongList';
import BarChart from '../components/BarChart';
import PieChart from '../components/PieChart';

class HomePage extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			data: [],
			categories: [],
		}
	}

	componentDidMount() {
		const props = this.props;

		const tempCategories = props.genres.map(x => x.name);
		let countArr = tempCategories.map(x => 0);
		props.songsByGenre.forEach(function (item, index, array) {
			countArr[props.genresMap[item.genre]] = item.genre_count;
		});
		
		this.setState({
			categories: tempCategories,
			data: countArr
		})
	
	}

	componentWillReceiveProps(newProps) {
		const tempCategories = newProps.genres.map(x => x.name);
		let countArr = tempCategories.map(x => 0);
		newProps.songsByGenre.forEach(function (item, index, array) {
			countArr[newProps.genresMap[item.genre]] = item.genre_count;
		});
		this.setState({
			categories: tempCategories,
			data: countArr
		})
	}

    render() {
		const { predictedGenre, genres } = this.props 
        return (
            <div id="container">
				<div style={{ marginBottom: '20px'}}>
					<Row gutter={16} style={{background: 'white', padding: '30px'}}>
						<Col md={{span: 24 }} lg={{ span: 8 }}>
							<Card 
								title="Upload your own songs" 
								bordered={false} 
								style={{minHeight: 260}} 
								headStyle={{fontSize: "1.5em"}}
							>
								<NormalSongUpload/>
								<div>
								{
									predictedGenre >= 0 ?
									(	<Row style={{ padding: '10px'}}>
											<h2>Your song was classified as <b>{genres[predictedGenre].name}</b> <Icon type="check" />
											</h2>
												<h2><i>Here's some other songs you might like:</i></h2>
										</Row>
									)
									:
									null
								}
								</div>
							</Card>
						</Col>
						<Col md={{span: 24 }} lg={{ span: 16 }}>
							<Card title="Suggested Songs" bordered={false} style={{ minHeight: 260 }} headStyle={{ fontSize: "1.5em" }}>
								<SongList />
							</Card>
						</Col>
					</Row>
				</div>
				<Row gutter={16} style={{ background: 'white', padding: '30px' }}>
					<Col lg={{ span: 23 }} xl={{ span: 12 }} style={{ background: 'white', padding: '30px' }}>
						<BarChart
							categories={this.state.categories}
							data={this.state.data}
							width={'100%'}
							height={400}
						/>
					</Col>
					<Col lg={{ span: 23 }} xl={{ span: 12 }} style={{ background: 'white', padding: '30px' }}>
						<PieChart
							categories={this.state.categories}
							data={this.state.data}
							width={'100%'}
							height={415}
						/>
					</Col>
				</Row>
            </div>
        )
    }
}

const mapStateToProps = state => {
	return {
		token: state.token,
		isAuthenticated: state.token != null,
		genres: state.genres,
		genresMap: state.genresMap,
		songsByGenre: state.songsByGenre,
		predictedGenre: state.predictedGenre,
		uploading: state.uploadingFile,
	}
  }

export default connect(mapStateToProps, null)(HomePage);
