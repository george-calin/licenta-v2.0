import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Collapse } from 'antd';
import MyCarousel from '../components/MyCarousel';
import GenreSelect from '../components/GenreSelect';
import SongSelect from '../components/SongSelect';
import BarChart from '../components/BarChart';
import PieChart from '../components/PieChart';
import MyTable from '../components/Table';
const { Panel } = Collapse;


class History extends React.Component {
    // componentDidUpdate() {
    //     if (!this.props.isAuthenticated)
    //     {
    //         this.props.history.push('/login/');
    //     }
    // }

    onClickPlay = value => {
        value = value !== undefined ? Number(value) : null;
        this.props.changeSelectedSong(value);
    }

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            categories: [],
            dataSource: [],
        }
    }

    componentDidMount() {
        if (!this.props.isAuthenticated)
        {
            this.props.history.push('/login/');
        }
        else 
        {
            const tempCategories = this.props.genres.map(x => x.name);
            const dataSource = []
            let countArr = tempCategories.map(x => 0);
            this.props.songs.forEach(function (item, index, array) {
                ++countArr[item.genre.genre_id];
                const dataSourceItem = {
                    key: index,
                    index: index,
                    title: item.name.substring(0, 50),
                    genre: item.genre.name,
                    songId: item.id
                }
                dataSource.push(dataSourceItem)
            });

            this.setState({
                categories: tempCategories,
                data: countArr,
                dataSource: dataSource
            })
        }
    }

    componentWillReceiveProps(newProps) {
        const tempCategories = newProps.genres.map(x => x.name);
        const dataSource = []
        let countArr = tempCategories.map(x => 0);
        newProps.songs.forEach(function (item, index, array) {
            ++countArr[item.genre.genre_id];
            const dataSourceItem = {
                key: index,
                index: index,
                title: item.name.substring(0, 50),
                genre: item.genre.name,
                songId: item.id
            }
            dataSource.push(dataSourceItem)
        });

        this.setState({
            categories: tempCategories,
            data: countArr,
            dataSource: dataSource
        })
    }
    
    render() {
        return (
            <div>
                <Row gutter={2} align="middle">
                    <Col lg={{ span:24}} xl={{ span: 9 }}  >
                        <Col lg={{span:24}} xl={{ span: 6}} style={{paddingTop: "10px"}}>
                            <label>
                                Select a genre:
                            </label>
                        </Col>
                        <Col lg={{span:24}} xl={{ span: 17}} style={{marginBottom: "10px", marginRight: "20px"}}>
                            <GenreSelect />
                        </Col>
                    </Col>
                    <Col lg={{ span: 24 }} xl={{span:9}}>
                        <Col lg={{span:24}} xl={{ span: 6}} style={{paddingTop: "10px"}}>
                            <label>
                                Search a song:
                            </label>
                        </Col>
                        <Col lg={{span:24}} xl={{ span: 17}} style={{marginBottom: "10px", marginRight: "20px"}}>
                            <SongSelect />
                        </Col>
                    </Col>
                </Row>
                <Row gutter={16} align="top" type="flex" style={{ marginBottom: "20px", background: 'white', padding: '30px', minHeight: "26.5vw"}}>
                    <Col lg={{ span: 24 }} xl={{ span: 14 }} style={{ marginTop: "10px"}}>
                        <MyCarousel/>
                    </ Col>
                    <Col lg={{ span: 24 }} xl={{ span: 10 }} style={{ marginTop: "10px" }}>
                        <MyTable columns={this.state.columns} dataSource={this.state.dataSource} />
                    </Col >
                </Row>
                <Collapse 
                    bordered={false} 
                    style={{ background: '#f9f9f9' }} 
                >
                    <Panel header="Your statistics" key="1" forceRender={true}>
                        <Row gutter={16}>
                            <Col lg={{ span: 23 }} xl={{ span: 12 }} style={{ background: 'white', padding: '30px' }}>
                                <BarChart 
                                    categories={this.state.categories}
                                    data={this.state.data} 
                                    width={'100%'} 
                                    height={400} 
                                />
                            </Col>
                            <Col lg={{span: 23 }} xl={{ span: 12 }} style={{ background: 'white', padding: '30px' }}>
                                <PieChart 
                                    categories={this.state.categories}
                                    data={this.state.data}
                                    width={'100%'}
                                    height={415}  
                                />
                            </Col>
                        </Row>
                    </Panel>
                </Collapse>
            </div>
        )
    }
}

const mapStateToProps = state => {
	return {
        token: 	state.token,
        isAuthenticated: state.token != null,
        genres: state.genres,
        songs: state.songs,
	}
}

export default connect(mapStateToProps, null)(History);

