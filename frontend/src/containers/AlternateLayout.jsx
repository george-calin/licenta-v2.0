import React from 'react';
import CustomHeader from '../components/Header'
import { Layout, Menu, Icon} from 'antd';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../store/actions/auth';
import PropTypes from 'prop-types';

const { Content, Footer, Sider } = Layout;

class AlternateLayout extends React.Component {

	static propTypes = {
		location: PropTypes.object.isRequired
	}

	render() {
		const { location } = this.props;
		return (
			<Layout>
				<CustomHeader
					{ ...this.props }
				/>


				<Content style={{ padding: '5 50px' }}>
					<Layout style={{ padding: '24px 0', background: '#fff' }}>

						{/* BEGIN SIDEBAR */}
						<Sider
							breakpoint="md"
							collapsedWidth="0"
							width={200}
							style={{backgroundColor: "#fff"}} >
							<Menu
								mode="inline"
								activeKey={location.pathname}
								selectedKeys={[location.pathname]}
								defaultSelectedKeys={[location.pathname]}
							>
								<Menu.Item key="/">
									<Link to="/">
										<span>
											<Icon type="home" />
											Home
										</span>
										</Link>
								</Menu.Item>
								<Menu.Item key="/history/">
									<Link to="/history/">
											<span>
												<Icon type="history" />
												Your Songs
											</span>
										</Link>
								</Menu.Item>
								<Menu.Item key="/login/">
									<Link to="/login/">
											<span>
												<Icon type="user"/>
												Account
											</span>
										</Link>
								</Menu.Item>
							</Menu>
						</Sider>
						{/* END SIDEBAR */}

						{/* BEGIN PAGE LEVEL CONTENT */}
						<Content style={{ padding: '0 24px', }}>
							<div style={{ width: "95%", background: '#f9f9f9', padding: 24, minHeight: 800 }}>
								{this.props.children}
							</div>
						</Content>
						{/* END PAGE LEVEL CONTENT */}

					</Layout>

				</Content>

				<Footer style={{ textAlign: 'center' }}></Footer>
			</Layout>
		);
	}
}

const mapStateToProps = state => {
    return {
        menuSelectedKey: state.menuSelectedKey
    }
}

const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(actions.logout())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AlternateLayout));
