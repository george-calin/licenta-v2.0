import React from 'react';
import {Row, Col, Icon, Spin} from 'antd';
import NormalLoginForm from '../components/LoginForm';
import NormalSignupForm from '../components/SignupForm';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

const loadingIcon = <Icon type="loading" style={{ fontSize: 45 }} spin />;

class LoginPage extends React.Component {

    render() {
        return (
            <Row >
                {
                this.props.loading ?

                <Col sm={{ span: 24}} lg={{span: 12}}>
                    <Spin style={{textAlign:"center"}} indicator={loadingIcon} />
                </Col>

                :

                <div>
                    <Col lg={{ span:8 }} offset={1} >
                        <h2>
                            Login
                        </h2>
                        <NormalLoginForm {...this.props}/>
                    </Col>
                    <Col lg={{ span:8 }} offset={1}>
                        <h2>
                            Sign-up
                        </h2>
                        <NormalSignupForm {...this.props}/>
                    </Col>
                </div>

                }
            </Row>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        loading: state.loading,
		error: state.error,
    }
}

export default withRouter(connect(mapStateToProps)(LoginPage));