import React from 'react';
import { Form, Icon, Input, Button, Spin, Alert } from 'antd';
import { connect } from 'react-redux';
import { NavLink, withRouter } from 'react-router-dom';
import * as actions from '../store/actions/auth';
import axios from 'axios';

const FormItem = Form.Item;
const loadingIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;


class NormalLoginForm extends React.Component {

  	handleSubmit = (e) => {
		e.preventDefault();
		this.props.form.validateFields((err, values) => {
			if (!err) {

				const username = values.userName;
				const password = values.password;

				this.props.authStart();

				setTimeout(() => {
					axios.post('http://127.0.0.1:8000/rest-auth/login/', {
						username: username,
						password: password
					})
					.then(res => {
						sessionStorage.setItem('username', username);
						const token = res.data.key;
						const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
						const data = {
							token: 			token,
							username: 		username,
							password: 		password,
							expirationDate: expirationDate
						}
						this.props.authLogin(data);
						this.props.history.push('/');

					})
					.catch(err => {
						this.props.authFail(err);
					})
				}, 200);

			}
	});
  }

  render() {
    let errorMessage = null;
    if (this.props.error) {
        errorMessage = (
			<Alert
				type="error"
				message="Wrong username or password"
			/>
        );
	}

	const { getFieldDecorator } = this.props.form;
    return (
        <div style={{width:"20%"}}>
            {
                this.props.loading ?

                <Spin style={{textAlign:"center"}} indicator={loadingIcon} />

                :

                <Form onSubmit={this.handleSubmit} className="login-form">

                    <FormItem>
                    {getFieldDecorator('userName', {
                        rules: [{ required: true, message: 'Please input your username!' }],
                    })(
						<Input
							prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
							placeholder="Username"
						/>,
                    )}
                    </FormItem>

                    <FormItem>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
						<Input
							prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
							type="password"
							placeholder="Password"
						/>,
					)}
                    </FormItem>

                    <FormItem>
                    <Button type="primary" htmlType="submit" style={{marginRight: '10px'}}>
                        Log in
                    </Button>
                    Or
                    <NavLink
                        style={{marginRight: '10px'}}
                        to='/signup/'> Signup
                    </NavLink>
                    </FormItem>
					{errorMessage}
                </Form>
			}
      </div>
    );
  }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm);

const mapStateToProps = (state) => {
    return {
        loading: state.loading,
		error: state.error,
    }
}

const mapDispatchToProps = dispatch => {
    return {
		authLogin: (username, password) => dispatch(actions.authLogin(username, password)),
		authStart: () => dispatch(actions.authStart()),
		authFail: (error) => dispatch(actions.authFail(error)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm));