import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import BaseRouter from './routes';
import 'antd/dist/antd.css';
import * as actions from './store/actions/auth';

import AlternateLayout from './containers/AlternateLayout'

class App extends Component {

	componentDidMount() {
		this.props.onTryAutoSignup();
		this.props.mountGenres();
		this.props.countSongsByGenre();
	}

  	render() {
		return (
			<div>
				<BrowserRouter>
					<AlternateLayout {...this.props}>
						<BaseRouter />
					</AlternateLayout>
				</BrowserRouter>
			</div>
		);
  	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.token !== null,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onTryAutoSignup: 	() => dispatch(actions.authCheckState()),
		mountGenres: 		() => dispatch(actions.getAllGenres()),
		countSongsByGenre: 	() => dispatch(actions.countSongsByGenre())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
