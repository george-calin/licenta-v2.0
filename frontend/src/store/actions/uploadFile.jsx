import axios from 'axios';
import * as actionTypes from './actionTypes';
import {getAllSongs, countSongsByGenre} from './auth';
import { BASE_URL, RM_LENGTH, MP3 } from '../utility';


export const tempUpload = (file) => {
	return {
		type: actionTypes.UPLOAD_FILE_TEMP,
		tempFile: file
	}
}

export const tempRemove = () => {
	return {
		type: actionTypes.UPLOAD_FILE_REMOVE,
		tempFile: null,
	}
}

export const uploadStart = () => {
	return {
		type: actionTypes.UPLOAD_FILE_START,
		uploadingFile: true,
		predictedGenre: -1,
	};
}

export const uploadSuccess = (genre_id) => {
	return {
		type: actionTypes.UPLOAD_FILE_SUCCESS,
		uploadingFile: false,
		errorFile: null,
		predictedGenre: genre_id
	};
}

export const uploadFail = error => {
	return {
		type: actionTypes.UPLOAD_FILE_FAIL,
		uploadingFile: false,
		errorFile: true,
	};
}

export const tempUploadFile = (file) => {
	return dispatch => dispatch(tempUpload(file));
}

export const tempUploadRemove = () => {
	return dispatch => dispatch(tempRemove());
}

export const recomendedSongs = (songs) => {
	return {
		type: actionTypes.RECOMENDED_SONGS,
		songs: songs
	}
}

export const uploadDocumentRequest = ({ file, token }) => {
    let data = new FormData();
	data.append('file', file);
	// placeholders
	data.append('genre', '1');
	data.append('genre_pk', '1');
	return dispatch => {
		dispatch(uploadStart());
		dispatch(recomendedSongs([]))

		// check if user is authenticated
		if (token) {
			axios.defaults.headers = {
				"Content-Type": "application/json",
				"Authorization": "Token " + token
			}
		}
		else {	delete axios.defaults.headers["Authorization"]
			axios.defaults.headers = {
				"Content-Type": "application/json",
			}
		}

		axios.post('http://127.0.0.1:8000/songs/create/', data)
		.then(res => {
			axios.get(`http://127.0.0.1:8000/songs/${res.data.id}`)
			.then(song => {
				var songData = song.data;
				var genre_id = songData.genre.genre_id;
				// console.log(song.data)

				dispatch(dispatch(tempRemove()));

				// loading false
				dispatch(uploadSuccess(genre_id));
				// get recomendations
				dispatch(getRandomSongs(token, genre_id));
				// refresh counters
				dispatch(countSongsByGenre());
				// refresh files
				if(token)
					dispatch(getAllSongs(token));	
			})
			.catch(error => dispatch(uploadFail()))
		})
		.catch(error => dispatch(uploadFail()))

	};
}

export const getRandomSongs = (token, genre_id) => {
	return dispatch => {
		if (token) {
			axios.defaults.headers = {
				"Content-Type": "application/json",
				"Authorization": "Token " + token
			}
		}
		else {
			axios.defaults.headers = {
				"Content-Type": "application/json",
			}
		}

		var URL = 'http://127.0.0.1:8000/songs/random/' + genre_id + '/'
		axios.get(URL, {
			"genre": genre_id
			})
			.then(res => {
				res.data.forEach((item) => {
					if (item.file != null)
						item.name = item.file.substr(RM_LENGTH).slice(0, -4);
					item.file = BASE_URL + item.name + MP3;
				});
				dispatch(recomendedSongs(res.data))
			})
	}
}