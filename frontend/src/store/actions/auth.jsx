import axios from 'axios';
import * as actionTypes from './actionTypes';
import { BASE_URL, RM_LENGTH, MP3 } from '../utility';


// Action Creators
// Action creators are exactly that—functions that create actions.
// It's easy to conflate the terms “action” and “action creator”,
// so do your best to use the proper term.

// In Redux, action creators simply return an action:

export const authStart = () => {
	return {
		type: actionTypes.AUTH_START
	}
}

export const authSuccess = (token, username) => {
	return {
		type: actionTypes.AUTH_SUCCESS,
		token: token,
		username: username,
        // songs: []
	}
}

export const authFail = error => {
	return {
		type: actionTypes.AUTH_FAIL,
		error: error
	}
}

export const signinFail = error => {
	return {
		type: actionTypes.SIGNIN_FAIL,
		error: error
	}
}

export const logout = () => {
	localStorage.removeItem('token');
	localStorage.removeItem('expirationDate');
	sessionStorage.removeItem('username');
	return {
		type: actionTypes.AUTH_LOGOUT,
		username: null,
		songsBackup: [],
		songs: []
	};
}

export const genresAll = (genres, genresMap) => {
	return {
		type: actionTypes.GENRES_ALL,
		genres: genres,
		genresMap: genresMap

	}
}


export const songsAll = (songs) => {
    return {
		type: actionTypes.SONGS_ALL,
		songsBackup: songs,
        songs: songs
    }
}

export const songsByGenre = (data) => {
	return {
		type: actionTypes.COUNT_SONGS_BY_GENRE,
		songsByGenre: data
	}
}

export const checkAuthTimeout = expirationTime => {
	return dispatch => {
		setTimeout(() => {
			dispatch(logout());
		}, expirationTime * 1000)
	}
}

export const authLogin = (data) => {
	return dispatch => {
		localStorage.setItem('token', data.token);
		localStorage.setItem('expirationDate', data.expirationDate);
		sessionStorage.setItem('username', data.username);

		dispatch(authSuccess(data.token, data.username));
        dispatch(getAllSongs(data.token));
		dispatch(checkAuthTimeout(3600));
	}
}


export const authSignup = (username, email, password1, password2) => {
	return dispatch => {
		dispatch(authStart());
		setTimeout( () => {
			axios.post('http://127.0.0.1:8000/rest-auth/registration/', {
					username: username,
					email: email,
					password1: password1,
					password2: password2
				})
				.then(res => {
					const token = res.data.key;
					const expirationDate = new Date(new Date().getTime() + 3600 * 1000);
					localStorage.setItem('token', token);
					localStorage.setItem('expirationDate', expirationDate);
					sessionStorage.setItem('username', username);
					dispatch(authSuccess(token, username));
					dispatch(getAllSongs(token));
					dispatch(checkAuthTimeout(3600));
				})
				.catch(err => {
					dispatch(authFail(err))
				})
		}, 200);
	}
}

export const authCheckState = () => {
	return dispatch => {
		const token = localStorage.getItem('token');
		const username = sessionStorage.getItem('username');
		if (token === undefined) {
			dispatch(logout());
		} else {
			const expirationDate = new Date(localStorage.getItem('expirationDate'));
			if (expirationDate <= new Date()) {
				dispatch(logout());
			} else {
                dispatch(authSuccess(token, username));
                dispatch(getAllSongs(token));
				dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
			}
		}
	}
}

export const getAllGenres  = () => {
	return dispatch => {
		axios.defaults.headers = {
			"Content-Type": "application/json",
		}
		axios.get('http://127.0.0.1:8000/songs/genres/')
		.then(res => {
			let newData = res.data.map( x => x.id)
			let genresMap = Object.keys(newData).reduce(function (obj, key) {
				obj[newData[key]] = key;
				return obj;
			}, {});
			dispatch(genresAll(res.data, genresMap))
		})
	}
}

export const getAllSongs = (token) => {
    return dispatch => {
		axios.defaults.headers = {
			"Content-Type": "application/json",
			"Authorization": "Token " + token
		}

        axios.get('http://127.0.0.1:8000/songs/')
        .then(res => {
            res.data.forEach( (item) => {
                if (item.file != null)
					item.name = item.file.substr(RM_LENGTH).slice(0, -4);
					item.file = BASE_URL + item.name + MP3;
            });
            dispatch(songsAll(res.data));
        })
    }
}

export const countSongsByGenre = () => {
	return dispatch => {
		axios.defaults.headers = {
			"Content-Type": "application/json",
		}

		axios.get('http://127.0.0.1:8000/songs/count/')
			.then(res => {
				dispatch(songsByGenre(res.data));
			})
	}
}

