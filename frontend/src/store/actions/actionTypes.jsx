export const SIGNIN_FAIL            = 'SIGNIN_FAIL';

export const AUTH_START 			= 'AUTH_START';
export const AUTH_SUCCESS 			= 'AUTH_SUCCESS';
export const AUTH_FAIL 				= 'AUTH_FAIL';
export const AUTH_LOGOUT 			= 'AUTH_LOGOUT';

export const UPLOAD_FILE_TEMP 		= 'UPLOAD_FILE_TEMP';
export const UPLOAD_FILE_REMOVE 	= 'UPLOAD_FILE_REMOVE';
export const UPLOAD_FILE_START 		= 'UPLOAD_FILE_START';
export const UPLOAD_FILE_SUCCESS 	= 'UPLOAD_FILE_SUCCESS';
export const UPLOAD_FILE_FAIL 		= 'UPLOAD_FILE_FAIL';
export const GENRES_ALL             = 'GENRES_ALL';
export const SONGS_ALL              = 'SONGS_ALL';

export const CHANGE_GENRE_FILTER    = 'CHANGE_GENRE_FILTER';
export const CHANGE_SONG_FILTER     = 'CHANGE_SONG_FILTER';

export const RECOMENDED_SONGS       = 'GET_RECOMENDED_SONGS';   
export const COUNT_SONGS_BY_GENRE   = 'COUNT_SONGS_BY_GENRE';