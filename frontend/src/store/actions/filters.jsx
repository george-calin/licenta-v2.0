import * as actionTypes from './actionTypes';

export const selectedGenre = (genre, songs) => {
    return {
        type: actionTypes.CHANGE_GENRE_FILTER,
        selectedGenre: genre,
        songs: songs,
        selectedSong: 0,
    }
}

export const selectedSong = val => {
    return {
        type: actionTypes.CHANGE_SONG_FILTER,
        selectedSong: val,
    }
}

export const changeSelectedGenre = (genre, songsBackup) => {
    return dispatch => {
        const songs = genre === null ? songsBackup : songsBackup.filter(song => song.genre.genre_id === genre);   
        dispatch(selectedGenre(genre, songs))};
}

export const changeSelectedSong = val => {
    return dispatch => {dispatch(selectedSong(val))};
}
