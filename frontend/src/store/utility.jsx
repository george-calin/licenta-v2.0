export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    }
}

export const BASE_URL = "http://127.0.0.1:8887/";
export const RM_LENGTH = "http://127.0.0.1:8000/media/".length;
export const MP3 = ".mp3";