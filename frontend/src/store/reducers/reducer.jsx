import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';


const initialState = {
    token: null,
	username: null,
    error: null,
    errorSignup: null,
	loading: false,
	errorFile: null,
	uploadingFile: false,
	tempFile: null,
    genres: [],
    genresMap: [],
    songsBackup: [],
    songs: [],
    predictedGenre: -1,
    recomendedSongs: [],
    selectedGenre: null,
    selectedSong: null,
    songsByGenre: [],
}


const authStart = (state, action) => {
    return updateObject(state, {
        error: null,
        errorSignup: null,
        loading: true
    });
}

const authSuccess = (state, action) => {
    return updateObject(state, {
        token: action.token,
        error: null,
        errorSignup: null,
		loading: false,
		username: action.username
    });
}

const signinFail = (state, action) => {
    return updateObject(state, {
        errorSignup: action.error,
        loading: false
    });
}

const authFail = (state, action) => {
    return updateObject(state, {
        error: action.error,
        loading: false
    });
}

const authLogout = (state, action) => {
    return updateObject(state, {
        token: null
    });
}

const tempUpload = (state, action) => {
    return updateObject(state, {
		tempFile: action.tempFile
    });
}

const tempRemove = (state, action) => {
    return updateObject(state, {
		tempFile: null,
    });
}

const uploadStart = (state, action) => {
    return updateObject(state, {
		errorFile: null,
        uploadingFile: true,
        predictedGenre: action.predictedGenre
    });
}

export function uploadSuccess(state, action ) {
	return  updateObject(state, {
		errorFile: false,
        uploadingFile: false,
        predictedGenre: action.predictedGenre
	});
  }

  export function uploadFail(state, action) {
	return updateObject(state, {
        uploadingFile: false,
        errorFile: true
	});
}

const genresAll = (state, action) => {
    return updateObject(state, {
        genres: action.genres,
        genresMap: action.genresMap
    })
}

const songsAll = (state, action) => {
	return updateObject(state, {
        songsBackup: action.songs,
		songs: action.songs
	})
}

const selectedGenre = (state, action) => {    
    return updateObject(state, {
        selectedGenre: action.selectedGenre,
        songs: action.songs,
        selectedSong: action.selectedSong
    })
}

const selectedSong = (state, action) => {
    return updateObject(state, {
        selectedSong: action.selectedSong
    })
}

const recomendedSongs = (state, action) => {
    return updateObject(state, {
        recomendedSongs: action.songs
    })
} 

const songsByGenre = (state, action) => {
    return updateObject(state, {
        songsByGenre: action.songsByGenre
    })
} 


const reducer = (state=initialState, action) => {
    switch (action.type) {
        case actionTypes.SIGNIN_FAIL:           return signinFail(state, action);
        case actionTypes.AUTH_START: 			return authStart(state, action);
        case actionTypes.AUTH_SUCCESS: 			return authSuccess(state, action);
        case actionTypes.AUTH_FAIL: 			return authFail(state, action);
		case actionTypes.AUTH_LOGOUT: 			return authLogout(state, action);
		case actionTypes.UPLOAD_FILE_TEMP: 		return tempUpload(state, action);
		case actionTypes.UPLOAD_FILE_REMOVE:	return tempRemove(state, action)
		case actionTypes.UPLOAD_FILE_START: 	return uploadStart(state, action);
        case actionTypes.UPLOAD_FILE_SUCCESS:   return uploadSuccess(state, action);
        case actionTypes.UPLOAD_FILE_FAIL: 		return uploadFail(state, action);
        case actionTypes.GENRES_ALL: 			return genresAll(state, action);
        case actionTypes.SONGS_ALL: 			return songsAll(state, action);
        case actionTypes.CHANGE_GENRE_FILTER: 	return selectedGenre(state, action);
        case actionTypes.CHANGE_SONG_FILTER: 	return selectedSong(state, action);
        case actionTypes.RECOMENDED_SONGS:      return recomendedSongs(state, action);
        case actionTypes.COUNT_SONGS_BY_GENRE:  return songsByGenre(state, action);
        default:
            return state;
    }
}

export default reducer;