from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

# for user in User.objects.all():
#     Token.objects.get_or_create(user=user)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Genre(models.Model):
    genre_id = models.IntegerField(null=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Song(models.Model):
    file = models.FileField(blank=False, null=True)
    genre = models.ForeignKey(Genre, on_delete=models.DO_NOTHING, null=True)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=True, null=True)
    def __str__(self):
        return self.file.name
    