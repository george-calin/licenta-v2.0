from django.urls import path

from .views import (
    SongUploadView,
    SongListView,
    SongRandomListView,
    SongDetailView,
    SongCreateView,
    SongUpdateView,
    SongDeleteView,
    SongGroupByGenreListView,
    GenreListView,
    GenreDetailView,
    GenreCreateView,
    GenreUpdateView,
    GenreDeleteView,
)


urlpatterns = [
    path('customView/', SongUploadView.as_view()),
    path('', SongListView.as_view()),
    path('random/<int:genre>/', SongRandomListView.as_view()),
    path('create/', SongCreateView.as_view()),
    path('<pk>', SongDetailView.as_view()),
    path('<pk>/update/', SongUpdateView.as_view()),
    path('<pk>/delete/', SongDeleteView.as_view()),
    path('count/', SongGroupByGenreListView.as_view()),
    path('genres/', GenreListView.as_view()),
    path('genres/create/', GenreCreateView.as_view()),
    path('genres/<pk>', GenreDetailView.as_view()),
    path('genres/<pk>/update/', GenreUpdateView.as_view()),
    path('genres/<pk>/delete/', GenreDeleteView.as_view()),
]
