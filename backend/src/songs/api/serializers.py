from rest_framework import serializers
from songs.models import Genre, Song
from django.contrib.auth.models import User



class SongSerializer(serializers.ModelSerializer):

    genre_pk = serializers.PrimaryKeyRelatedField(
        queryset = Genre.objects.all(), source='genre', write_only=True
    )

    class Meta:
        depth = 1
        model = Song
        fields = ('id', 'file', 'genre', 'user', 'genre_pk')

class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = "__all__"


class GroupByGenreSerializer(serializers.Serializer):
    genre = serializers.IntegerField()
    genre_count = serializers.IntegerField()