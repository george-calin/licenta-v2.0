from songs.models import Song, Genre
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes
from .serializers import SongSerializer, GenreSerializer, GroupByGenreSerializer
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, serializers
from ml_api.songToData import startGetGenreProcess
from django.db.models import Q, Count
# from django.contrib.auth.models.AnonymousUser
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView, 
    CreateAPIView,
    DestroyAPIView,
    UpdateAPIView,
)


class SongGroupByGenreListView(ListAPIView):
    authentication_classes = ()
    queryset = Song.objects.values('genre').annotate(genre_count=Count('genre'))
    serializer_class = GroupByGenreSerializer

# @permission_classes()
class SongRandomListView(ListAPIView):
    authentication_classes = ()
    queryset = Song.objects.all()
    serializer_class = SongSerializer

    def get_queryset(self):
        genre_id = self.kwargs.get('genre')
        # print(sekf.request.data)
        # get the genre object
        genre_obj = Genre.objects.get(genre_id=genre_id)
        
        if self.request.user:
            queryset = Song.objects.filter(~Q(user__id=self.request.user.id)).filter(genre=genre_obj).order_by('?')[:12]
        else:
            queryset = Song.objects.filter(genre=genre_obj).order_by('?')[:12]

        return queryset
        
# @permission_classes((, ))
class SongUploadView(APIView):
    permission_classes = ()
    authentication_classes = ()
    # queryset = Song.objects.all()
    # serializer_class = SongSerializer
    parser_class = (FileUploadParser, )

    def post(self, request, *args, **kwargs):
        file_serializer = SongSerializer(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((IsAuthenticated, ))
class SongListView(ListAPIView):
    # authentication_classes = (TokenAuthentication, )
    queryset = Song.objects.all()
    serializer_class = SongSerializer

    def get_queryset(self):
        user = self.request.user
        return Song.objects.filter(user__id=user.id)


@permission_classes(())
class SongDetailView(RetrieveAPIView):
    authentication_classes = ()
    queryset = Song.objects.all()
    serializer_class = SongSerializer


# @permission_classes((IsAuthenticated, ))
class SongCreateView(CreateAPIView):
    user = serializers.PrimaryKeyRelatedField(
        read_only=True,
    )
    queryset = Song.objects.all()
    serializer_class = SongSerializer

    def perform_create(self, serializer):
        if serializer.is_valid():
            filename = self.request.data['file'].name

            # Save entry
            user = self.request.user
            if user.id == None:
                file = serializer.save(user=None)
            else:
                file = serializer.save(user=self.request.user)

            # get the genre id
            genre_id = startGetGenreProcess(filename)
            # get the genre object
            genre_obj = Genre.objects.get(genre_id=genre_id)
            
            # updated previous saved entry with the genre
            Song.objects.filter(id=file.id).update(genre=genre_obj)

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        

@permission_classes((IsAuthenticated, ))
class SongUpdateView(UpdateAPIView):
    # authentication_classes = (TokenAuthentication, )
    queryset = Song.objects.all()
    serializer_class = SongSerializer


@permission_classes((IsAuthenticated, ))
class SongDeleteView(DestroyAPIView):
    # authentication_classes = (TokenAuthentication, )
    queryset = Song.objects.all()
    serializer_class = SongSerializer


@permission_classes(())
class GenreListView(ListAPIView):
    # authentication_classes = (TokenAuthentication, )
    queryset = Genre.objects.all().order_by('genre_id')
    serializer_class = GenreSerializer


@permission_classes((IsAuthenticated, ))
class GenreDetailView(RetrieveAPIView):
    # authentication_classes = (TokenAuthentication, )
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


@permission_classes((IsAuthenticated, ))
class GenreCreateView(CreateAPIView):
    # authentication_classes = (TokenAuthentication, )
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


@permission_classes((IsAuthenticated, ))
class GenreUpdateView(UpdateAPIView):
    # authentication_classes = (TokenAuthentication, )
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


@permission_classes((IsAuthenticated, ))
class GenreDeleteView(DestroyAPIView):
    # authentication_classes = (TokenAuthentication, )
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer

