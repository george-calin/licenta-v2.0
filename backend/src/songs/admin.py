from django.contrib import admin

from .models import Genre, Song

admin.site.register(Song)
admin.site.register(Genre)
