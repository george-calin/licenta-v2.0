# -*- coding: utf-8 -*-
import eyed3

# Remove logs
eyed3.log.setLevel("ERROR")


def isMono(filename):
    audiofile = eyed3.load(filename)
    return audiofile.info.mode == 'Mono'


def getGenre(filename):
    audiofile = eyed3.load(filename)
    # No genre
    if not audiofile.tag.genre:
        return None
    else:
        return audiofile.tag.genre.name.encode('utf-8')


# isMono("/home/george/licenta/backend/src/media/26._Joy_Crookes_-_Lover_Don't.mp3")
# 26._Joy_Crookes_-_Lover_Dont.mp3
