# -*- coding: utf-8 -*-
from subprocess import Popen, PIPE, STDOUT
import os
import eyed3

from .sliceSpectrogram import sliceSpectrogram
from .audioFilesTools import isMono
from .config import rawDataPath, spectrogramsPath, pixelPerSecond
from .predictTools import get_genre

# HARDCODING ( FORGIVE ME )
genres = ['Rap', 'Electronic', 'Pop', 'Rock', 'Classical', 'Reggae']


# Tweakable parameters
desiredSize = 128

# Define
currentPath = os.path.dirname(os.path.realpath(__file__))

# Remove logs
eyed3.log.setLevel("ERROR")


def createSpectrogramFromAudio(filename):

    # Create temporary mono track if needed
    if isMono(rawDataPath + filename):
        command1 = "cp '{}' '/tmp/{}'".format(rawDataPath + filename, filename)
    else:
        command1 = "sox '{}' '/tmp/{}' remix 1,2".format(
            rawDataPath + filename, filename)
    p = Popen(command1, shell=True, stdin=PIPE, stdout=PIPE,
              stderr=STDOUT, close_fds=True, cwd=currentPath)
    output, errors = p.communicate()
    if errors:
        print(errors)

    # Create spectrogram
    filename = filename.replace(".mp3", "")

    # -Y	Sets the target total height of the spectrogram(s)
    # -X	X-axis pixels/second;
    # −m	Creates a monochrome spectrogram (the default is colour).
    # −r 	Raw spectrogram: suppress the display of axes and legends.
    # -o	Name of the spectrogram output PNG file, default ‘spectrogram.png’. If ‘-’ is given,
    # 		the spectrogram will be sent to standard output (stdout).
    command2 = "sox '/tmp/{}.mp3' -n spectrogram -Y 200 -X {} -m -r -o '{}.png'".format(
        filename, pixelPerSecond, spectrogramsPath+filename)
    p = Popen(command2, shell=True, stdin=PIPE, stdout=PIPE,
              stderr=STDOUT, close_fds=True, cwd=currentPath)
    output, errors = p.communicate()
    if errors:
        print(errors)

    try:
        # Remove tmp mono track
        os.remove("/tmp/{}.mp3".format(filename))
    except:
        print(command1, command2)


def formatFilename(filename):
    # toBeReplaced = [" ("," )"," '", ' -', ' &', "(",")","'", '-', '&']
    # for ch in toBeReplaced:
    #     filename = filename.replace(ch, '')
    # filename = filename.replace(" ", "_")
    # filename = filename.strip()
    filename = filename.replace(" ", "_")
    filename = filename.replace("&", "")
    filename = filename.replace("(", "")
    filename = filename.replace(")", "")
    filename = filename.replace("'", "")
    return filename


def startGetGenreProcess(filename=None):
    filename = formatFilename(filename)

    print("Creating spectrograms...")
    createSpectrogramFromAudio(filename)
    print("Spectrograms created!")

    print("Creating slices...")
    sliceSpectrogram(filename, desiredSize)
    print("Slices created!")

    print("Predicting genre")
    genre = get_genre(filename)
    print("Genre predicted, it's {} !".format(genres[genre]))

    return genre
