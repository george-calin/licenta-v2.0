# Import Pillow:
from PIL import Image
import os.path

from .config import spectrogramsPath, slices_path

# Slices all spectrograms


def createSlicesFromSpectrograms(desiredSize):
    for filename in os.listdir(spectrogramsPath):
        if filename.endswith(".png"):
            sliceSpectrogram(filename, desiredSize)

# Creates slices from spectrogram
# TODO Improvement - Make sure we don't miss the end of the song


def sliceSpectrogram(filename, desiredSize):
    # genre = filename.split("_")[0] 	#Ex. Dubstep_19.png

    # Create spectrogram
    filename = filename.replace(".mp3", ".png")

    # Load the full spectrogram
    img = Image.open(spectrogramsPath + filename)

    # Compute approximate number of 128x128 samples
    width, height = img.size
    nbSamples = int(width / desiredSize)
    width - desiredSize

    # For each sample
    for i in range(nbSamples):
        print("Creating slice: ", (i + 1), "/", nbSamples, "for", filename)
        # Extract and save 128x128 sample
        startPixel = i * desiredSize
        imgTmp = img.crop((startPixel, 1, startPixel +
                           desiredSize, desiredSize + 1))
        imgTmp.save(slices_path + "/{}_{}.png".format(filename[:-4], i))
