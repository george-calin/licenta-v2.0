# -*- coding: utf-8 -*-
from PIL import Image
import numpy as np

# Returns numpy image at size img_size*img_size


def getProcessedData(img, img_size):
    img = img.resize((img_size, img_size), resample=Image.ANTIALIAS)
    img_data = np.asarray(img, dtype=np.uint8).reshape(img_size, img_size, 1)
    img_data = img_data / 255.
    return img_data

# Returns numpy image at size img_size*img_size


def getImageData(filename, img_size):
    img = Image.open(filename)
    img_data = getProcessedData(img, img_size)
    return img_data
