# -*- coding: utf-8 -*-
import os
import numpy as np

from PIL import Image
from statistics import mode
from .model import createModel
from .config import slices_path, slice_size


# List genres
# genres = os.listdir(slices_path)
# # genres = [filename for filename in genres if os.path.isdir(
# #     slices_path + filename)]

# HARDCODING ( FORGIVE ME )
genres = ['Dance', 'EDM', 'Electronic', 'Pop', 'Rock', 'Classical']
nbClasses = len(genres)

# Create model
model = createModel(nbClasses, slice_size)
# Load weights
print("     [+] Loading weights...")
model.load('ml_api/musicDNN.tflearn')
# model.load('musicDNN.tflearn')
print("     Weights loaded! ✅")


def get_image_data(filename, img_size):
    """ Returns numpy image at size img_size*img_siz """

    img = Image.open(filename)

    img = img.resize((img_size, img_size), resample=Image.ANTIALIAS)
    img_data = np.asarray(img, dtype=np.uint8).reshape(img_size, img_size, 1)
    img_data = img_data / 255.

    return img_data


def create_temp_dataset(filename):
    """ creates temporary dataset for the uploaded song """
    print(filename)
    data = []
    filenames = os.listdir(slices_path)
    filenames = list(filter(lambda file:
                            file.startswith(filename) and file.endswith('.png'), filenames))
    for file in filenames:
        img_data = get_image_data(slices_path + file, slice_size)
        data.append(img_data)
    X = np.array(data).reshape([-1, slice_size, slice_size, 1])

    return X


def get_genre(filename):
    """ this func. does what is says """
    filename = filename.replace('.mp3', '')
    X = create_temp_dataset(filename)
    labels = model.predict_label(X)
    predicted_genre = mode(labels[:, 0])
    print("     [+] this is this genre: {} ".format(predicted_genre))
    return predicted_genre
