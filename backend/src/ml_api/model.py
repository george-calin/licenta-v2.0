# -*- coding: utf-8 -*-

import numpy as np

import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression


# tflearn.layers.conv.conv_2d (	incoming
# 								,nb_filter
# 								,filter_size
# 								,strides=1
# 								,padding='same'
# 								,activation='linear'
# 								,bias=True
# 								,weights_init='uniform_scaling'
# 								,bias_init='zeros'
# 								,regularizer=None
# 								,weight_decay=0.001
# 								,trainable=True
# 								,restore=True
# 								,reuse=False
# 								,scope=None
# 								,name='Conv2D'
# 								)

def createModel(nbClasses, imageSize):
	print("[+] Creating model...")
	convnet = input_data(shape=[None, imageSize, imageSize, 1], name='input') # (128, 128, 1)

	convnet = conv_2d(convnet, nb_filter=64, filter_size=2, activation='elu', weights_init="Xavier") # (128, 128, 64)
	convnet = max_pool_2d(convnet, kernel_size=2, strides=2) # (64, 64, 64)

	convnet = conv_2d(convnet, 128, 2, activation='elu', weights_init="Xavier") # ( 64, 64, 128)
	convnet = max_pool_2d(convnet, 2) # (32, 32, 128)

	convnet = conv_2d(convnet, 256, 2, activation='elu', weights_init="Xavier") # (32, 32, 256)
	convnet = max_pool_2d(convnet, 2)  # (16, 16, 256)

	convnet = conv_2d(convnet, 512, 2, activation='elu', weights_init="Xavier") # (16, 16, 512)
	convnet = max_pool_2d(convnet, 2) # (8, 8, 512)

	convnet = fully_connected(convnet, 1024, activation='elu') # ( 1024 )
	convnet = dropout(convnet, 0.5)

	convnet = fully_connected(convnet, nbClasses, activation='softmax') # ( nbClasses)
	convnet = regression(convnet, optimizer='rmsprop', loss='categorical_crossentropy')

	model = tflearn.DNN(convnet)
	print("    Model created! ✅")
	return model