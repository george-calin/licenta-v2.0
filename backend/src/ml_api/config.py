# Define paths for files
spectrogramsPath = "/home/george/licenta/backend/src/ml_api/Data/Spectrograms/"
slices_path = "/home/george/licenta/backend/src/ml_api/Data/Slices/"
datasetPath = "/home/george/licenta/backend/src/ml_api/Data/Dataset/"
rawDataPath = "/home/george/licenta/backend/src/media/"

# Spectrogram resolution
pixelPerSecond = 50

# Slice parameters
slice_size = 128

# Dataset parameters
filesPerGenre = 1000
validationRatio = 0.3
testRatio = 0.1

# Model parameters
batchSize = 128
learningRate = 0.001
nbEpoch = 20
